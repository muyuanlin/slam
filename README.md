# Robot trajectory optimization using LMA C++ library.
This C++ project aims to optimize robot trajectories with multiple constraints from sensors, such as GPS, IMU, altimeter, and so on. Trajectory optimization has crutial applications in robotics.

## Papers Describing the Approach:
Lovegrove, Steven, Alonso Patron-Perez, and Gabe Sibley. "Spline Fusion: A continuous-time representation for visual-inertial fusion with application to rolling shutter cameras." BMVC. 2013.

Ramadasan, Datta. SLAM temporel à contraintes multiples. Diss. Université Blaise Pascal-Clermont-Ferrand II, 2015.

## Requirements
This project requires LMA (Levenberg-Marquardt Algorithm), Eigen, Sophus and Geodesy. Also, it is build in ROS. Please refer to the following websites:  
1. ROS: http://wiki.ros.org/
2. LMA: https://github.com/bezout/LMA

    If you have the following issue when compile this project:
    ```
    Could not find a package configuration file provided by "libv" with any of
    the following names:  

    libvConfig.cmake  
    libv-config.cmake  
    ```
    Please execute command in the terminal:
    ```
    export CMAKE_PREFIX_PATH=/.../ROOT
    ```
    here, "/.../ROOT" is the directory where you build and install LMA. 
    
3. Eigen: http://eigen.tuxfamily.org/index.php?title=Main_Page  
4. Sophus: https://github.com/strasdat/Sophus  
```
sudo apt-get install ros-kinetic-sophus
```

5. Geodesy: http://wiki.ros.org/geodesy  
```
sudo apt-get install ros-kinetic-geodesy  
```

## Structure
The project folder "./slam" is a package under ROS workspace. Under the folder "/slam", there are two directories: '/slam/src', '/slam/include', and two configuration files: 'CMakeLists.txt', 'package.xml'.  

For that moment, I have uploaded a "main.cpp" under "/slam/src" which includes three threads (optimizatioin of spline trajectories within five seconds window). The optimization module is under folder '/slam/include/'.






