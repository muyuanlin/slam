//==============================================================================
//         Copyright 2015 INSTITUT PASCAL UMR 6602 CNRS/Univ. Clermont II
//
//          Distributed under the Boost Software License, Version 1.0.
//                 See accompanying file LICENSE.txt or copy at
//                     http://www.boost.org/LICENSE_1_0.txt
//==============================================================================

#ifdef WIN32
#include <random>
#include <vector>
#include <cmath>
#endif

#include <libv/lma/lma.hpp>
#include <eigen3/Eigen/Dense>
#include <boost/math/special_functions/sinc.hpp>
#include <sophus/se3.hpp>
#include <slam/se3_spline.h>
#include <slam/readsensor.h>
#include <slam/constraint.h>
#include <slam/lmasolver.h>


int main(int argc , char** argv)
{
    ros::init(argc, argv, "main");
    std::vector<gps_data> gps;
    std::vector<imu_data> imu;

    ros::Duration duration(5.0);
    getData(duration, gps, imu);

	// Trajectory settings 
    size_t num_knots = 10; 
    double dt = 0.5;

	ros::NodeHandle node;

	
	sensor mySensor(node);
	optimization myOptimization(num_knots, dt, gps, imu, &mySensor);
	
	// ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &sensor::callback, &mySensor);
	ros::Timer timer = node.createTimer(ros::Duration(1), &optimization::run, &myOptimization, true);

	ros::AsyncSpinner spinner(2);
	spinner.start();
	ros::waitForShutdown();

    return 0;
}
