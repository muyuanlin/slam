//
// Created by Muyuan Lin on 2016-07-25.
//

#ifndef LMASOLVER_H
#define LMASOLVER_H

#include <vector>
#include <ros/ros.h>
#include <ros/time.h>

#include <libv/lma/lma.hpp>
#include <eigen3/Eigen/Dense>

#include <slam/se3_spline.h>
#include <slam/readsensor.h>
#include <slam/constraint.h>

typedef SE3Group<double> SE3Type;
#define MINIMUM_DATA_NUM 5
#define KNOTS_ADD_NUM 1

namespace v
{
    Eigen::Matrix3d rotation_exp(const Eigen::Matrix3d &a)
    {
      double theta2 = a(0,1)*a(0,1) + a(0,2)*a(0,2) +  a(1,2)*a(1,2) + std::numeric_limits<double>::epsilon();
      double theta = std::sqrt(theta2);
      return Eigen::Matrix3d::Identity() + boost::math::sinc_pi(theta)*a+(1.0-std::cos(theta))/theta2*a*a;
     }
        
     void apply_rotation(Eigen::Matrix3d &rotation, const Eigen::Vector3d &d)
     {
       Eigen::Matrix3d skrew;
       skrew << 0, -d.z(), d.y(), d.z(), 0, -d.x(), -d.y(), d.x(), 0;
       rotation *= rotation_exp(skrew);
      }

     void apply_small_rotation(Eigen::Matrix3d &rotation, double h, int i, int j)
     {
         const Eigen::Vector3d col_l = rotation.col(j) - rotation.col(i)*h;
         rotation.col(i) += rotation.col(j) * h;
         rotation.col(j) = col_l;
     }

     void apply_small_rotation_x(Eigen::Matrix3d& rotation, double h) {apply_small_rotation(rotation, h, 1, 2);}
     void apply_small_rotation_y(Eigen::Matrix3d& rotation, double h) {apply_small_rotation(rotation, -h, 0, 2);}
     void apply_small_rotation_z(Eigen::Matrix3d& rotation, double h) {apply_small_rotation(rotation, h, 0, 1);}

}

namespace lma
{
    template<> struct Size<Pose> {enum {value = SE3Type::num_parameters-1};};

    template<int I> void apply_small_increment(Pose &obj, double d, v::numeric_tag<I>, const Adl&){
        if (I < 3){
            Eigen::Matrix<double, 3, 3> R = obj.rotationMatrix();

            if      (I == 0) v::apply_small_rotation_x(R, d);
            else if (I == 1) v::apply_small_rotation_y(R, d);
            else if (I == 2) v::apply_small_rotation_z(R, d);

            obj.data[0] = R(2, 1); 
            obj.data[1] = R(0, 2);
            obj.data[2] = R(1, 0);
        }
        else{
            obj.data[I] += d;
        }
    } 

    void apply_increment(Pose &obj, const double increment[SE3Type::num_parameters-1], const Adl&){
        Eigen::Matrix3d R = obj.rotationMatrix();

        v::apply_rotation(R, Eigen::Vector3d(increment[0], increment[1], increment[2]));
        obj.data[0] = R(2, 1); 
        obj.data[1] = R(0, 2);
        obj.data[2] = R(1, 0);

        for (size_t i=3; i<6;  ++i){
            obj.data[i] += increment[i]; 
        }

    }
}

class optimization
{
public:
   	size_t num_knots;
	double dt;
	std::vector<gps_data> gps;
	std::vector<imu_data> imu;
	std::vector<Pose> parameter_blocks;

    sensor *Sensor;

	optimization(size_t num_knots_, double dt_, std::vector<gps_data> &gps_, std::vector<imu_data> &imu_, sensor *Sensor_): num_knots(num_knots_), dt(dt_), gps(gps_), imu(imu_), Sensor(Sensor_){}

	void run(const ros::TimerEvent& event);	
};

void optimization::run(const ros::TimerEvent& event)
{
	// check if received enough sensor data
	if (gps.size() < MINIMUM_DATA_NUM){
		std::cout << "Not enough data for optimization!" <<endl;
		return;
	}
	
	for (size_t i = 0; i < num_knots; i++){
		Eigen::Matrix<double, 6, 1> vec;
		vec <<0, 0, 0, gps[0].x, gps[0].y, gps[0].z;
		Pose p = Pose(vec);
		parameter_blocks.push_back(p);
	}
	
	double lambda = 0.001;
	double iteration_max = 10;
	double offset = gps[0].time; 
    	int opt_num = 0;
   
    while (Sensor->gps.size() <= MINIMUM_DATA_NUM);//{std::cout << gps.size() <<" "<< Sensor->gps.size() <<" " <<Sensor->imu.size()<<endl;}

    while (Sensor->gps.size() > MINIMUM_DATA_NUM){
        // Initialize the solver
        lma::Solver<GPSConstraint, IMUConstraint> solver(lambda, iteration_max);
        
        // Add constraint
        for(size_t i = 0 ; i < gps.size() ; ++i){
            int i0 = checkInterval(num_knots, offset, dt, gps[i].time);
            if (i0 == -1)
                continue;
            else{
                solver.add(GPSConstraint(gps[i], dt, offset+i0*dt, 1.0), &parameter_blocks[opt_num + i0-1], &parameter_blocks[opt_num + i0], &parameter_blocks[opt_num + i0+1], &parameter_blocks[opt_num + i0+2] ); 
                }	
           
        }
     std::cout <<"OK, !" <<endl;     
        for(size_t i = 0 ; i < imu.size() ; i+=5){
            int i0 = checkInterval(num_knots, offset, dt, imu[i].time);
            if (i0 == -1)
                continue;
            else{
                solver.add(IMUConstraint(imu[i], dt, offset+i0*dt, 1.0),&parameter_blocks[opt_num + i0-1], &parameter_blocks[opt_num + i0], &parameter_blocks[opt_num + i0+1], &parameter_blocks[opt_num + i0+2] ); 
                } 
        }

        solver.solve(lma::DENSE,lma::enable_verbose_output());

        // Visualize pose

        // Read new data from Sensor
        int num_new_gps = MINIMUM_DATA_NUM;
        int num_new_imu = num_new_gps * Sensor->imu.size()/ Sensor->gps.size(); 
        for (size_t i=0; i<num_new_gps; i++){
            gps_data tmp = Sensor->gps[i];
            Sensor->gps.erase(Sensor->gps.begin());
            gps.push_back(tmp);
            gps.erase(gps.begin());
        }
                     
        for (size_t i=0; i<num_new_imu; i++){
            imu_data tmp = Sensor->imu[i];
            Sensor->imu.erase(Sensor->imu.begin());
            imu.push_back(tmp);
            imu.erase(imu.begin());
        }
        
        offset = gps[0].time; 
        opt_num += KNOTS_ADD_NUM;

        double x0 = gps.back().x;
        double y0 = gps.back().y;
        double z0 = gps.back().z;
        Pose ptmp = parameter_blocks.back(); 
        double a0 = ptmp.data[0];
        double b0 = ptmp.data[1]; 
        double c0 = ptmp.data[2]; 
        for (size_t i = 0; i < KNOTS_ADD_NUM; i++){
            Eigen::Matrix<double, 6, 1> vec;
            vec <<a0, b0, c0, x0, y0, z0;
            Pose p = Pose(vec);
            parameter_blocks.push_back(p);
        }

    }

	
}
#endif
